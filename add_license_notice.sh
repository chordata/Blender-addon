#!/bin/bash


FILES=$(find scripts/addons/chordata -type f -name "*.py" \
		-not -path "*/requests/*" \
		-not -path "*/dnspython/*" \
		-not -path "*/coop_server/*" )

NOTICE=$(cat _license_notice.txt)

for FILE in $FILES ; do
	cp $FILE $FILE.tmp
	cat _license_notice.txt $FILE.tmp > $FILE
	rm $FILE.tmp
	
done

