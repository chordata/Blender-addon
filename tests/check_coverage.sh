#!/bin/bash


if [ $# -lt 1 ]; then
	echo 'Usage: ./check_coverage.sh <Blender binary path>'
	exit 1
fi

BLENDER_BIN=$1
command -v ${BLENDER_BIN}


if [ "$?" != "0" ]; then
	echo Blender binary path \[$1\] not valid
	exit 1
fi

set -eu

SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
source "$SCRIPTPATH/get_blender_python_bin.sh"

BLENDPY=$(get_bundled_python_dir ${BLENDER_BIN})
BUNDLED_PY=$(get_bundled_python ${BLENDPY})

if [ "$?" != "0" ]; then
	echo $BUNDLED_PY
	exit 1
fi 

${BUNDLED_PY} check_coverage.py