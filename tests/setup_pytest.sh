#!/bin/bash


if [ $# -lt 1 ]; then
	echo 'Usage: ./setup_pytest.sh <Blender binary path> [--with-ipython]'
	exit 1
fi

BLENDER_BIN=$1
command -v ${BLENDER_BIN}

if [ "$?" != "0" ]; then
	echo Blender binary path \[$1\] not valid
	exit 1
fi

set -eu

SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
source "$SCRIPTPATH/get_blender_python_bin.sh"

BUNDLED_PY=$(${BLENDER_BIN} -b --python get_blender_python_bin.py | grep "~~~")

if [ "$?" != "0" ]; then
	echo Error getting blender\'s bundled py: $BUNDLED_PY
	exit 1
fi 

BUNDLED_PY=$(echo $BUNDLED_PY | sed 's/~~~ //g')
echo found blender\'s python interpreter: $BUNDLED_PY
# exit

${BUNDLED_PY} -c 'print("Bundled Python interpreter working")'

${BUNDLED_PY} -m ensurepip

TARGET_DIR=$(${BLENDER_BIN} -b --python-expr "import sys; print('~~~', sys.path[1])" | grep "~~~")
TARGET_DIR=$(echo $TARGET_DIR | sed 's/~~~ //g')
echo installing to target dir: $TARGET_DIR

PIP="${BUNDLED_PY} -m pip"

echo found blender\'s python pip: $PIP

# version 20.0 is wroken
# https://github.com/pypa/pip/issues/7217
${PIP} install --upgrade "pip>=22.0.4"

${PIP} install pandas --target=$TARGET_DIR
${PIP} install scipy --target=$TARGET_DIR
${PIP} install sklearn --target=$TARGET_DIR

${PIP} install pytest --target=$TARGET_DIR

${PIP} install pytest-mock --target=$TARGET_DIR

${PIP} install pytest-cov --target=$TARGET_DIR

if [ $# -eq 2 ] && [ "$2" == "--with-ipython" ]; then
	${PIP} install ipython
	${BUNDLED_PY} -c "import IPython; print('ipython sucesfully installed')"
fi

${BUNDLED_PY} -c "import pytest; print('pytest sucesfully installed')"
