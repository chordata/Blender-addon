import pytest

# DATA
test_data = {
    'tag': "number",
    'text': "5",
    'attributes': {
            'key': "value",
    }
}

test_data_noattr = {
    'tag': "number",
    'text': "5",
}

test_data_onlytag = {
    'tag': "number",
}

test_data_emptytag = {
    'tag': ""
}

test_data_empty = {}


@pytest.fixture
def xml_generator(chordata_module):
    return chordata_module.ops.xml_generator


@pytest.fixture
def generate_xml_element(chordata_module):
    return chordata_module.ops.xml_generator.generate_xml_element


def test_valid_XML_element_generation(generate_xml_element):
    """Generate_xml_element function.
    Test with complete valid input"""
    res = generate_xml_element(test_data)
    assert test_data['tag'] == res.tag
    assert test_data['text'] == res.text
    assert test_data['attributes'] == res.attrib


def test_empty_XML_element_generation(generate_xml_element):
    """Test empty input"""
    with pytest.raises(KeyError, match=r".*dict passed"):
        generate_xml_element(test_data_empty)


@pytest.fixture
def generate_xml(chordata_module):
    return chordata_module.ops.xml_generator.generate_xml


configurations_dict = {
    'Configuration': {
        'tag': "Configuration",
        'KC_revision': {
            'tag': "KC_revision",
            'text': "2"
        },
        'Communication': {
            'tag': "Communication",
            'Adapter': {
                'tag': "Adapter",
                'text': "/dev/i2c-1"
            },
            'Ip': {
                'tag': "Ip",
                'text': "192.168.85.123"
            },
            'Port': {
                'tag': "Port",
                'text': "6565"
            },
            'Log': {
                'tag': "Log",
                'text': "stdout,file"
            },
            'Transmit': {
                'tag': "Transmit",
                'text': "osc"
            },
            'Send_rate': {
                'tag': "Send_rate",
                'text': "50"
            },
            'Verbosity': {
                'tag': "Verbosity",
                'text': "0"
            }
        },
        'Osc': {
            'tag': "Osc",
            'Base': {
                'tag': "Base",
                'text': "/Chordata"
            }
        },
        'Fusion': {
            'tag': "Fusion",
            'Beta_start': {
                'tag': "Beta_start",
                'text': "1.0"
            },
            'Beta_end': {
                'tag': "Beta_end",
                'text': "0.2"
            },
            'Time': {
                'tag': "Time",
                'text': "5000"
            }
        }
    }
}

import re


class NotochordUnicastSettingsMock():
    local_address = "127.0.0.1"
    transmission_mode = 'UNICAST'
    local_osc_port = "7000"
    kc_revision = '2'
    log = 'qualcosa'
    send_rate = 50
    verbosity = '0'
    adapter = 'qualcosaltro'
    osc_base = '/Chordata'


class NotochordBroadcastSettingsMock():
    local_address = "127.0.0.1"
    transmission_mode = 'BROADCAST'
    local_osc_port = "7000"
    net_submask = "255.255.255.0"
    kc_revision = '2'
    log = 'qualcosa'
    send_rate = 50
    verbosity = '0'
    adapter = 'qualcosaltro'
    osc_base = '/Chordata'


class NotochordMulticastSettingsMock():
    local_address = "127.0.0.1"
    transmission_mode = 'MULTICAST'
    local_osc_port = "7000"
    multi_id_group = "239.0.0.1"
    kc_revision = '2'
    log = 'qualcosa'
    send_rate = "50"
    verbosity = '0'
    adapter = 'qualcosaltro'
    osc_base = '/Chordata'


def test_generate_configurations_dict(bpy_test, xml_generator):
    notochord_settings = NotochordUnicastSettingsMock()
    assert xml_generator.generate_configurations_dict(
        notochord_settings)['Configuration']['Communication']['Port']['text'] == notochord_settings.local_osc_port
    notochord_settings = NotochordBroadcastSettingsMock()
    assert xml_generator.generate_configurations_dict(
        notochord_settings)['Configuration']['Communication']['Port']['text'] == notochord_settings.local_osc_port
    assert xml_generator.generate_configurations_dict(
        notochord_settings)['Configuration']['Communication']['Ip']['text'] == notochord_settings.net_submask
    notochord_settings = NotochordMulticastSettingsMock()
    assert xml_generator.generate_configurations_dict(
        notochord_settings)['Configuration']['Communication']['Port']['text'] == notochord_settings.local_osc_port
    assert xml_generator.generate_configurations_dict(
        notochord_settings)['Configuration']['Communication']['Ip']['text'] == notochord_settings.multi_id_group


def test_generate_valid_XML(bpy_test, xml_generator):
    notochord_settings = NotochordUnicastSettingsMock()
    t = bpy_test.data.texts.new("test_armature_xml")
    t.write('<Armature>Armature</Armature>')
    res = xml_generator.generate_xml(
        notochord_settings, t)
    assert type(res) == str
    print (res)
    assert re.match(r"(<!--.*-->)*\W*<Chordata", res) is not None
    sendrate = re.search(r"<Send_rate>(.*)</Send_rate>", res)
    assert sendrate is not None
    assert res.find("<Armature>") != -1
    assert res.find("</Armature>") != -1

import xml.etree.ElementTree as ET


def test_armature_datablock(bpy_test, xml_generator):
    t = bpy_test.data.texts.new("test_armature_xml")
    t.write('<Armature>Armature</Armature>')
    armature_xml = xml_generator.load_armature_xml(t)
    assert type(armature_xml) == ET.Element

def test_load_armature_xml_wrong_argument(xml_generator):
    some_arg = 1324
    with pytest.raises(TypeError):
        xml_generator.load_armature_xml(some_arg)

def test_load_armature_xml_string(xml_generator):
    some_string = "<Armature><somenode></somenode></Armature>"
    armature_xml = xml_generator.load_armature_xml(some_string)
    assert type(armature_xml) == ET.Element
    assert len(armature_xml.findall('somenode')) == 1
    

def test_invalid_armature_datablock(bpy_test, xml_generator):
    t = bpy_test.data.texts.new("test_armature_xml")
    with pytest.raises(ValueError):
        armature_xml = xml_generator.load_armature_xml(t)
