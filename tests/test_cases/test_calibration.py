import pytest
import mathutils
import math

@pytest.fixture
def calibration_module():
    from chordata.ops import calibration_manager
    return calibration_manager

@pytest.fixture
def calib_manager(calibration_module):
    return calibration_module.Calibration_Manager()


def test_calibration_store_packet(bpy_test, calib_manager, copp_Q_packet, copp_RAW_packet, copp_server):
    assert calib_manager.entries == []
    packet_length = len(copp_RAW_packet._get_elements())
    
    calib_manager.phase = 1 #enable the storage of packets
    
    calib_manager.store_packet(copp_Q_packet)
    calib_manager.store_packet(copp_RAW_packet)
    
    assert len(calib_manager.entries) == packet_length 
    assert calib_manager.samples == 1
    
    copp_Q_packet.restore()
    copp_RAW_packet.restore()
    copp_Q_packet.time = copp_server.oscparse.OSCtimetag(0,5678)
    copp_RAW_packet.time = copp_server.oscparse.OSCtimetag(0,5678)

    calib_manager.store_packet(copp_Q_packet)
    calib_manager.store_packet(copp_RAW_packet)
    
    assert len(calib_manager.entries) == packet_length * 2 
    assert calib_manager.samples == 2

    calib_manager.free()
    assert calib_manager.entries == []

    
def test_calibration_entry_append_diff_len(bpy_test, calib_manager, copp_Q_packet, copp_RAW_packet, calibration_module):
    copp_RAW_packet._elements.pop()
    with pytest.raises(calibration_module.Calibration_error):
        calib_manager.append_entry(copp_Q_packet, copp_RAW_packet)


def test_pose_calibration_import():
    from calibration import core