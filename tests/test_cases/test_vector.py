import pytest

def remove_all_objs(bpy):
	for ob in bpy.data.objects:
		bpy.data.objects.remove(ob)


@pytest.fixture
def vecnode(bpy_test, engine, chordata_nodetree):
	vectornode = chordata_nodetree.nodes.new("VectorNodeType")
	bpy_test.ops.object.empty_add()
	vectornode.settings.target_object = bpy_test.context.object

	vec_eng_node = engine.EngineNode(vectornode)
	yield vec_eng_node
	remove_all_objs(bpy_test)


def test_vector_node_no_ob(bpy_test, engine, chordata_nodetree):
	vectornode = chordata_nodetree.nodes.new("VectorNodeType")
	vec_eng_node = engine.EngineNode(vectornode)
	assert vec_eng_node.arrow is None

def test_vector_node_point_cloud(base_fake_nodetree, bpy_test, engine, chordata_nodetree, copp_RAW_packet):
	t_node, n_node, f_node = base_fake_nodetree
	engine_instance = engine.Engine()

	engine_root = engine_instance.parse_tree(n_node)

	vectornode = chordata_nodetree.nodes.new("VectorNodeType")
	bpy_test.ops.object.empty_add()
	vectornode.settings.target_object = bpy_test.context.object
	vectornode.settings.draw_point_cloud = True
	vec_eng_node = engine.EngineNode(vectornode)
	assert vec_eng_node.point_cloud in bpy_test.data.objects

	engine_root(copp_RAW_packet)
	vec_eng_node.id_settings.selected_subtarget = "head"
	copp_RAW_packet.restore()
	engine_root(copp_RAW_packet)

	vec_eng_node.id_settings.selected_subtarget
	vec_eng_node.cleanup()
	assert vec_eng_node.point_cloud not in bpy_test.data.objects

	remove_all_objs(bpy_test)

def test_vector_node_cleanup(bpy_test, vecnode):
	assert vecnode.arrow in bpy_test.data.objects
	vecnode.cleanup()
	assert vecnode.arrow not in bpy_test.data.objects

def test_vector_node(chordata_nodetree, bpy_test, engine, base_fake_nodetree, copp_RAW_packet):
	"""Crear faketree, 
	crear nodo_vector y conectarlo al n_node,
	pasar paquetes al engine pero pasar propiedades a vector
	"""

	v_node = chordata_nodetree.nodes.new('VectorNodeType')
	bpy_test.ops.object.empty_add()
	v_node.settings.target_object = bpy_test.context.object
	#vec_eng_node = engine.EngineNode(v_node)
	t_node, n_node, f_node = base_fake_nodetree
	socket_in = v_node.inputs[0]
	socket_out = n_node.outputs[0]
	chordata_nodetree.links.new(socket_in, socket_out)

	engine_instance = engine.Engine()
	engine_root = engine_instance.parse_tree(n_node)
	engine_root(copp_RAW_packet)
 	
 	#Referencia de tipo engine node
	for child in engine_root.children:
		if child.name == v_node.name:
			vec_eng_node = child
			arrow = bpy_test.data.objects[ vec_eng_node.arrow ]
			prev_quat = arrow.rotation_quaternion.copy() 	
			break

	assert "head" in engine_root.id_tree.subtargets
	assert "dorsal" in engine_root.id_tree.subtargets
	assert "base" in engine_root.id_tree.subtargets
	

	copp_RAW_packet.restore()
	vec_eng_node.id_settings.selected_subtarget = "head"

	engine_root(copp_RAW_packet)
	assert arrow.rotation_quaternion != prev_quat

	vec_eng_node.id_settings.vec_from_raw = "CUSTOM"
	vec_eng_node.id_settings.custom_vec_offset = 9
	copp_RAW_packet.restore()
	engine_root(copp_RAW_packet)
	assert vec_eng_node.id_settings.offset_in_bounds == False


