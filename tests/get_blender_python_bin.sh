#!/bin/bash

set -eu

BLENDPY=""
PY_BIN_DIR=bin
PY_BIN_NAME=python3.7m
PIP_BIN_NAME=pip3

function get_bundled_python_dir() {

	BLENDPY=$($1 --background | grep "found bundled python" | sed -e "s/found\s*bundled\s*python\s*:\s*//g")

	if [ -z "$BLENDPY" ]; then
		echo "Bundled python not found, aborting script"
		return 1
	else
		echo $BLENDPY
		return 0
	fi

}


function get_bundled_python() {
	echo "$1/$PY_BIN_DIR/$PY_BIN_NAME"

}

function get_bundled_pip() {
	echo "$1/$PY_BIN_DIR/$PIP_BIN_NAME"
}