# Chordata client (Blender addon)  
# -- Motion capture manager for the Chordata Open Source motion capture system
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2019 Bruno Laurencich
# Copyright 2020-2022 Bruno Laurencich, Lorenzo Micozzi Ferri
#
# This file is part of Chordata client (Blender addon).
#
# Chordata client (Blender addon) is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata client (Blender addon).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

import bpy
from .. import defaults
from ..utils import gui
from .. import dependencies


class AddChordataAvatar(bpy.types.Operator, dependencies.DepsCheckMixin):
    bl_idname = "chordata.avatar_add"
    bl_label = "Add a Mocap avatar to the scene"

    @classmethod
    def poll(cls, context):
        return True

    def execute(self, context):
        if not self.check_deps(): return {'CANCELLED'}
        
        filepath = defaults.TEMPLATES_FILE
        link = False 

        with bpy.data.libraries.load(filepath) as (data_from, data_to):
            data_to.objects = [defaults.ARMATURE_NAME, defaults.AVATAR_MODEL_NAME]

        for ob in data_to.objects:
            if ob is not None:
                ob.is_chordata_item = True
                context.collection.objects.link(ob)

        for nodetree in defaults.ARM_NODETREE_NAMES:
            bpy.ops.chordata.configuration_add('EXEC_DEFAULT', treename = nodetree )

            arm_node = bpy.data.node_groups[nodetree].nodes['Armature Node']
            for ob in data_to.objects:
                if ob.name == defaults.ARMATURE_NAME:
                    arm_node.settings.armature_ob = ob
                    break

        return {'FINISHED'}


class AddChordataKCBase(bpy.types.Operator, dependencies.DepsCheckMixin):
    @classmethod
    def poll(cls, context):
        return True

    def invoke(self, context, event):
        if not dependencies.check_import():
            return context.window_manager.invoke_props_dialog(self, width=600)

        return self.execute(context)

    def execute(self, context):
        if not self.check_deps(): return {'CANCELLED'}
            
        filepath = defaults.TEMPLATES_FILE
        link = False 

        with bpy.data.libraries.load(filepath) as (data_from, data_to):
            data_to.objects = [self.model_name]

        for ob in data_to.objects:
            if ob is not None:
                ob.is_chordata_item = True
                context.collection.objects.link(ob)


        bpy.ops.chordata.configuration_add('EXEC_DEFAULT', treename = defaults.CUBE_NODETREE_NAME )

        test_cube_node = bpy.data.node_groups[defaults.CUBE_NODETREE_NAME].nodes['Test Cube Node']
        test_cube_node.settings.target_object = data_to.objects[0]

        return {'FINISHED'}

class AddChordataKCR1(AddChordataKCBase):
    bl_idname = "chordata.kceptor_add"
    bl_label = "Add a KCeptorR1 model to the scene"
    model_name = defaults.KC_MODEL_NAME
    
class AddChordataKCpp(AddChordataKCBase):
    bl_idname = "chordata.kceptorpp_add"
    bl_label = "Add a KCeptor++ model to the scene"
    model_name = defaults.KCPP_MODEL_NAME

class AddChordataFakeArm(bpy.types.Operator, dependencies.DepsCheckMixin):
    bl_idname = "chordata.fake_arm_add"
    bl_label = "Add fake arm objects to the scene"

    @classmethod
    def poll(cls, context):
        return True

    def execute(self, context):
        filepath = defaults.TEMPLATES_FILE
        link = False 

        with bpy.data.libraries.load(filepath) as (data_from, data_to):
            data_to.collections = [defaults.FAKE_BONES_COLLECTION]

        for col in data_to.collections:
            if col is not None:
                col.is_chordata_item = True
                context.scene.collection.children.link(col)
                for ob in col.objects:
                    ob.is_chordata_item = True
                    # context.collection.objects.link(ob)



        return {'FINISHED'}

class AddChordataActions(bpy.types.Operator):
    bl_idname = "chordata.actions_add"
    bl_label = "Add pre-recorded actions to the blend file"

    @classmethod
    def poll(cls, context):
        return True

    def execute(self, context):
        filepath = defaults.TEMPLATES_FILE
        link = False 
        sufx = defaults.DEMO_ANIM_SUFX  

        with bpy.data.libraries.load(filepath) as (data_from, data_to):
            data_to.actions = [ac for ac in data_from.actions if sufx+ac not in bpy.data.actions]

        for ac in data_to.actions:
            if ac is not None:
                ac.is_chordata_item = True
                ac.use_fake_user = True
                ac.name = sufx + ac.name  

        return {'FINISHED'}


class RemoveChordataObjects(bpy.types.Operator):
    bl_idname = "chordata.objects_rm"
    bl_label = "Remove Chordata objects from the scene"

    @classmethod
    def poll(cls, context):
        return True

    def execute(self, context):
        for ob in bpy.data.objects:
            if ob.is_chordata_item:
                bpy.data.objects.remove(ob)
        
        for col in bpy.data.collections:
            if col.is_chordata_item:
                bpy.data.collections.remove(col)

        for ac in bpy.data.actions:
            if ac.is_chordata_item:
                bpy.data.actions.remove(ac)

        for tx in bpy.data.texts:
            if tx.is_chordata_item:
                bpy.data.texts.remove(tx)


        return {'FINISHED'}


bpy.types.Object.is_chordata_item = bpy.props.BoolProperty(name = "Chordata object", 
    description = "Flag: was this object created by the chordata addon?", default = False)

bpy.types.Collection.is_chordata_item = bpy.props.BoolProperty(name = "Chordata collection", 
    description = "Flag: was this collection created by the chordata addon?", default = False)

bpy.types.Action.is_chordata_item = bpy.props.BoolProperty(name = "Chordata collection", 
    description = "Flag: was this action created by the chordata addon?", default = False)

bpy.types.Text.is_chordata_item = bpy.props.BoolProperty(name = "Chordata collection", 
    description = "Flag: was this action created by the chordata addon?", default = False)


class AddChordataTree(bpy.types.Operator):
    bl_idname = "chordata.configuration_add"
    bl_label = "Add default Chordata configurations"

    workspace: bpy.props.StringProperty(name = "Workspace", 
    description = "Workspace where to add the configuration tree", default = defaults.WORKSPACE_NAME)

    overwrite: bpy.props.BoolProperty(name = "Set default", 
    description = "Overwrite the current configuration with the default one?", default = False)

    treename: bpy.props.StringProperty(name = "Configuration name", 
    description = "Name of the Chordata Configuration Datablock to add.", default = defaults.NODETREE_NAME)

    @classmethod
    def poll(cls, context):
        return True

    def execute(self, context):
        filepath = defaults.TEMPLATES_FILE
        link = False 

        bpy.ops.chordata.workspace_add('EXEC_DEFAULT')
        bpy.ops.chordata.actions_add('EXEC_DEFAULT')

        try:
            editors = gui.nodeeditor_spaces_get(self.workspace)
            if not editors:
                self.report({'WARNING'}, "No ChordataNodeTree editor found on %s" % self.workspace )
                return {'CANCELLED'}
            
            if self.overwrite and self.treename in bpy.data.node_groups.keys():
                bpy.data.node_groups[self.treename].name = "old_%s" % self.treename

            if self.treename not in bpy.data.node_groups.keys():
                with bpy.data.libraries.load(filepath) as (data_from, data_to):
                    data_to.node_groups = [self.treename]

            for ed in editors:
                ed.node_tree = bpy.data.node_groups[self.treename]
            
        except KeyError as e:
            self.report({"ERROR"}, str(e))
            return {'CANCELLED'}

        return {'FINISHED'}
        
class AddChordataTextFiles(bpy.types.Operator):
    bl_idname = "chordata.text_add"
    bl_label = "Add Chordata text datablocks"

    @classmethod
    def poll(cls, context):
        return True

    def set_string_on_text(self, name, s):

        if name in bpy.data.texts:
            t = bpy.data.texts[name]
        else:
            t = bpy.data.texts.new(name)
        
        t.from_string(s)


    def execute(self, context):

        with open(defaults.CUSTOM_FILE) as f:
            s = f.read()
            s = s.replace("from ..", "from chordata.")
            s = s.replace("from .", "from chordata.nodes.")

            custom_node_name = "Chordata custom node"
            self.set_string_on_text(custom_node_name, s)

        with open(defaults.DEF_BIPED_FILE) as f:
            s = f.read()
            n = s.find("<Armature>")
            if n == -1:
                self.report({'WARNING'}, "The armature file is not valid")
                return {'CANCELLED'}

            arm_text_name = "Chordata default biped armature.xml"
            self.set_string_on_text(arm_text_name, s[n:])


        return {'FINISHED'}

to_register = (AddChordataAvatar, AddChordataKCR1, AddChordataKCpp,
               AddChordataFakeArm, AddChordataActions, 
               RemoveChordataObjects, AddChordataTree, AddChordataTextFiles)