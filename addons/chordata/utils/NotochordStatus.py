class NotochordStatus:
    IDLE = 0
    CHECKING_CONNECTION = 1
    SENDING_CONFIGURATION = 2
    ACTIVATING_CONFIGURATION = 3
    INITIALIZING = 4
    LISTENING = 5
    STOPPING = 6

    class Manager:
        _status = None
        @property
        def status(self):
            return self.get_status()
        @status.setter
        def status(self, value):
            self.set_status(value)
        
        @staticmethod
        def get_status():
            cls = NotochordStatus.Manager
            if cls._status is None:
                cls._status = NotochordStatus.IDLE
            return cls._status
        @staticmethod
        def set_status(value):
            cls = NotochordStatus.Manager
            if value < NotochordStatus.IDLE or value > NotochordStatus.STOPPING:
                raise ValueError("Invalid status")
            cls._status = value
