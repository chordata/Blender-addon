# Chordata client (Blender addon)  
# -- Motion capture manager for the Chordata Open Source motion capture system
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2019 Bruno Laurencich
# Copyright 2020-2022 Bruno Laurencich, Lorenzo Micozzi Ferri
#
# This file is part of Chordata client (Blender addon).
#
# Chordata client (Blender addon) is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata client (Blender addon).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

import bpy
from ..utils import gui
# from .. import defaults
# from collections import deque
from .engine import DataTarget
from os import path
from calibration.core import Calibrator
from calibration.utils import Output as Calib_Output
from calibration.defaults import __version__ as calib_version
from ..defaults import (Calibration_error, Calibration_Phase, 
                        DOING_CALIB_MASK, DONE_CALIB_MASK,
                        ADDON_VER_STR, GIT_HASH)
import pandas as pd
import numpy as np
from ..copp_server.osc4py3_buildparse.oscbuildparse import timetag2float
# import numpy as np
from datetime import datetime
import mathutils

def _output(text, *args):
	print(text, *args)
	
class Calibration_Manager:
	def __init__(self):
		self.free()
				
	def free(self):
		self.entries = []
		self.incoming_packets = {}
		self.samples = 0
		self.phase = Calibration_Phase.NONE
		self.indexes = {
				'vert_i' : None,
				'vert_s' : None,
				'func_arms_i' : None,
				'func_arms_s' : None,
				'func_trunk_i' : None,
				'func_trunk_s' : None,
				'func_legs_r_i' : None,
				'func_legs_r_s' : None,
				'func_legs_l_i' : None,
				'func_legs_l_s' : None
			}
		self._last_packet = False
	
	def check_phase(self, phase):
		return phase in self.phase
		
	# def check_doing(phase):
	#     return phase in self.doing
	
	def set_phase(self, calib_settings):
		if calib_settings.doing_static_calib:
			if not self.check_phase(Calibration_Phase.VERT):
				self.phase |= Calibration_Phase.VERT
				self.indexes['vert_i'] = self.samples
				_output(">> Start calibration STATIC at sample: {}".format(self.samples))
			
			# doing calibration phase
		else:
			if self.check_phase(Calibration_Phase.VERT):
				self.phase &= ~Calibration_Phase.VERT
				self.phase |= Calibration_Phase.DONE_VERT
				self.indexes['vert_s'] = self.samples
				self._last_packet = True
				_output("<< End calibration STATIC at sample: {}".format(self.samples))
				
		# ----------- RIGHT_LEG --------------        
				
		if calib_settings.doing_r_leg_calib:
			if not self.check_phase(Calibration_Phase.RIGHT_LEG):
				self.phase |= Calibration_Phase.RIGHT_LEG
				self.indexes['func_legs_r_i'] = self.samples
				_output(">> Start calibration RIGHT LEG at sample: {}".format(self.samples))
			
			# doing calibration phase
		else:
			if self.check_phase(Calibration_Phase.RIGHT_LEG):
				self.phase &= ~Calibration_Phase.RIGHT_LEG
				self.phase |= Calibration_Phase.DONE_RIGHT_LEG
				self.indexes['func_legs_r_s'] = self.samples
				self._last_packet = True
				_output("<< End calibration RIGHT LEG at sample: {}".format(self.samples))
				
		# ----------- LEFT LEG --------------        
				
		if calib_settings.doing_l_leg_calib:
			if not self.check_phase(Calibration_Phase.LEFT_LEG):
				self.phase |= Calibration_Phase.LEFT_LEG
				self.indexes['func_legs_l_i'] = self.samples
				_output(">> Start calibration LEFT LEG at sample: {}".format(self.samples))
			
			# doing calibration phase
		else:
			if self.check_phase(Calibration_Phase.LEFT_LEG):
				self.phase &= ~Calibration_Phase.LEFT_LEG
				self.phase |= Calibration_Phase.DONE_LEFT_LEG
				self.indexes['func_legs_l_s'] = self.samples
				self._last_packet = True
				_output("<< End calibration LEFT LEG at sample: {}".format(self.samples))
						
		# ----------- TRUNK --------------        
				
		if calib_settings.doing_trunk_calib:
			if not self.check_phase(Calibration_Phase.TRUNK):
				self.phase |= Calibration_Phase.TRUNK
				self.indexes['func_trunk_i'] = self.samples
				_output(">> Start calibration TRUNK at sample: {}".format(self.samples))
			
			# doing calibration phase
		else:
			if self.check_phase(Calibration_Phase.TRUNK):
				self.phase &= ~Calibration_Phase.TRUNK
				self.phase |= Calibration_Phase.DONE_TRUNK
				self.indexes['func_trunk_s'] = self.samples
				self._last_packet = True
				_output("<< End calibration TRUNK at sample: {}".format(self.samples))

		# ----------- ARMS --------------        
				
		if calib_settings.doing_arms_calib:
			if not self.check_phase(Calibration_Phase.ARMS):
				self.phase |= Calibration_Phase.ARMS
				self.indexes['func_arms_i'] = self.samples
				_output(">> Start calibration ARMS at sample: {}".format(self.samples))
			
			# doing calibration phase
		else:
			if self.check_phase(Calibration_Phase.ARMS):
				self.phase &= ~Calibration_Phase.ARMS
				self.phase |= Calibration_Phase.DONE_ARMS
				self.indexes['func_arms_s'] = self.samples
				self._last_packet = True
				_output("<< End calibration ARMS at sample: {}".format(self.samples))
		
		# save the state to blender prop
		calib_settings.states = int(self.phase)

	def store_packet(self, packet, force = False):
		if not force and not (DOING_CALIB_MASK & self.phase) and not self._last_packet:
			return False
		
		if packet.time not in self.incoming_packets.keys():
			self.incoming_packets[packet.time] = {}
			
		self.incoming_packets[packet.time][packet.target] = packet
		
		# if timestamp has the packets Q and RAW
		if len(self.incoming_packets[packet.time].keys()) == 2:
			# create a new entry
			self.append_entry(self.incoming_packets[packet.time][DataTarget.Q], 
							  self.incoming_packets[packet.time][DataTarget.RAW])
			# delete the stored packet references, they wont't be used anymore
			del self.incoming_packets[packet.time]
			
			# keep count of the number of samples (whole body readings of both DataTargets)
			self.samples += 1
			
			self._last_packet = False
			return True
		
		return False
	
	def append_entry(self, q_packet, raw_packet):
		assert q_packet.time == raw_packet.time, "The received packets have diferent timetags"
		
		_entries = {}
		
		i_q = 0
		for msg in q_packet.get():
			_entries[msg.subtarget] = msg.payload    
			i_q += 1
		
		i_raw = 0
		for msg in raw_packet.get():
			if msg.subtarget not in _entries.keys():
				raise Calibration_error("The key {} in RAW packet was not present in the Q packet".format(msg.subtarget))
			_entries[msg.subtarget] += msg.payload
			i_raw += 1
		
		if i_q != i_raw:
			raise Calibration_error("The number of messages on the Q and RAW packet differ ")
		
		for label, payload in _entries.items(): 
			# timestamp =timetag2float(q_packet.time) * 1000 
			timestamp = int(q_packet.time.frac / 1000)
			self.entries.append((timestamp, label) + payload)
			# self.entries.append((timetag2float(q_packet.time) * 1000, 'label') + payload)
	
	def data_ready(self):
		return self.phase & DONE_CALIB_MASK == DONE_CALIB_MASK
	
	def run(self, calib_settings):
		assert self.data_ready(), "All the calibration phases should be done before running the calibration"
		try:
			df = pd.DataFrame(self.entries, columns=Calibrator.required_columns)

   
			log_calib = True
			now_str = datetime.now().strftime("%Y-%m-%dT%H-%M-%S")
			if  bpy.data.is_saved:
				log_calib = path.join(path.dirname(bpy.data.filepath), "{}-Chordata_calibration_result.log".format(now_str))
			
			Calib_Output.instance = None #reset calibrator logging
			c = Calibrator(
				df,
				calib_settings.g_s,
				calib_settings.a_s,
				calib_settings.m_s,
				self.indexes,
				calib_settings.fc,
				log=log_calib
			)
			
			c.run(calib_settings.sta_fac_l, calib_settings.sta_fac_r )
			self.out = {
				"time": now_str,
				"version": calib_version,
				"addon" : ADDON_VER_STR+GIT_HASH,
				"calib_results": c.results,
				# "calib_intermediate_results": c._intermediate_results,
				"indexes": c.section_indexes,
				# "raw_indexes": c.raw_section_indexes,
				"STA_factors": {"left": calib_settings.sta_fac_l, 
                    			"right": calib_settings.sta_fac_r},
				"delta_time" : c.dt,
				"g_s" : calib_settings.g_s,
				"a_s" : calib_settings.a_s,
				"m_s" : calib_settings.m_s,
				"fc" : calib_settings.fc
			}

			if isinstance(log_calib, str):
				msg = "Calib Log file saved as: {}".format(log_calib)
				c.out.log(msg)
				gui.console_write( msg )
			
		except Exception as e:
			raise Calibration_error("Error running pose-calibration algorithm, see console for details")
		
		finally:
			# stop the calibration phase
			self.phase &= ~DONE_CALIB_MASK

		return c.results["heading_rot"], c.results["post_rot"], now_str
	
	def run_sta_correction(self, left_sta_f, right_sta_f, bone_l = 'l-upperarm', bone_r = 'r-upperarm'):
		if not getattr(self, "out", None):
			return

		r = self.out['calib_results']

		q_l = Calibrator.sta_correction(r['gamma'][bone_l], r['gyro_int'][bone_l], left_sta_f)
		q_r = Calibrator.sta_correction(r['gamma'][bone_r], -r['gyro_int'][bone_r], right_sta_f)
  
		self.out["STA_factors"] = {"left": left_sta_f, "right": right_sta_f}
		self.out["calib_results"]["heading_rot"][bone_l] = q_l
		self.out["calib_results"]["heading_rot"][bone_r] = q_r
	
		return q_l, q_r
 
	def dump_entries_DF(self, path):
		df = pd.DataFrame(self.entries, columns=Calibrator.required_columns)
		df.to_csv(path, index = False)
		msg = "Calib data CSV file saved as: {}".format(path)
		print(msg)
		gui.console_write(msg)
		
	def dump_calib_data(self, path):
		if getattr(self, "out", None):
			import json
			import numpy
			import copy
   
			_out = copy.deepcopy(self.out)
			
			def convert_to_tuples(main_key):
				for k1,val1 in self.out[main_key].items():
					if isinstance(val1, dict):
						for k2,val2 in self.out[main_key][k1].items():
							prev_type = type(val2)
	   
							if isinstance(val2, (mathutils.Quaternion, np.ndarray)):
								_out[main_key][k1][k2] = tuple(val2)
							
			
			convert_to_tuples("calib_results")
   
			for k,i in self.out["indexes"].items():
				_out["indexes"][k] = int(i)
			
			# write out file
			out_str = json.dumps(_out, indent=4)
			with open(path, 'w') as file:
				file.write(out_str)
    
			msg = "Calib results JSON file saved as: {}".format(path)
			print(msg)
			gui.console_write(msg)

