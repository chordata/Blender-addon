# Chordata client (Blender addon)  
# -- Motion capture manager for the Chordata Open Source motion capture system
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2019 Bruno Laurencich
# Copyright 2020-2022 Bruno Laurencich, Lorenzo Micozzi Ferri
#
# This file is part of Chordata client (Blender addon).
#
# Chordata client (Blender addon) is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata client (Blender addon).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

import bpy
import functools
import json
from sys import path
from mathutils import Quaternion
from os.path import join, abspath, dirname

from .base import BaseChordataNodeOperator
from ..copp_server import COPP_Server, COPP_Common_packet, COPP_Message, oscparse
from ..utils.networking import get_local_ip
from ..utils import out, gui
from . import xml_generator, notochord_ops
from ..defaults import TEXTBLOCK_XML_NAME, POSE_SPACE_G_SUFX, Network_error
from .engine import Engine, EngineNode, DataTarget, ID_data_settings_getter

import time
import xml.etree.ElementTree as XML

# def connect_request(xml, notochord_address):
#     url = 'http://' + notochord_address + '/configuration'
#     headers = {'content-type': 'xml'}
#     r = requests.get(url, headers=headers, data=xml)


class NotochordReceiveOperator(bpy.types.Operator, BaseChordataNodeOperator):
    """Parses tree and start connection with Notochord"""
    bl_idname = "chordata.notochord_receive"
    bl_label = "Notochord Receive Operator"

    def _execute(self, context):
        # config_file = self.id_settings.config_file

        # if not config_file:
        #     self.report({'ERROR'}, "Empty armature file pointer")
        #     return False

        self.copp_server = COPP_Server( port = self.id_settings.local_osc_port,
                                        logger = out.get_logger() )
        
        self.copp_server.set_base_osc_addr( self.id_settings.osc_base )
        self.copp_server.start()

        return True

    def _modal(self, context, event):
        for packet in self.copp_server.get():
            packet.session_id = self.packet_counter 
            self.engine.engine_root(packet)  
            self.packet_counter +=1

    def _cancel(self, context):
        if hasattr(self, 'copp_server'):
            self.copp_server.close_and_wait()
        return True

from ..utils.AsyncHTTP import AsyncHTTP

def get_configuration_xml(
    kc_revision, adapter,
    ip, port,
    log, transmit,
    send_rate, verbosity,
    osc_base, hierarchy
):
    def build_kceptors(branch, level=0):
        if len(branch) < 1:
            return ""
        return f'<k_ceptor Name="{branch[0][1]}" id="2">{hex(branch[0][0])}{build_kceptors(branch[1], level + 1)}</k_ceptor>'
    
    if hierarchy is None:
        if str(kc_revision) == "2": hierarchy = ("0x73", tuple())
        else: hierarchy = ("0x70", tuple())
        
    return (
        f"<chordata version=\"1.0.0\">"
            f"<configuration>"
                f"<kc_revision>{kc_revision}</kc_revision>"
                f"<communication>"
                    f"<adapter>{adapter}</adapter>"
                    f"<ip>{ip}</ip>"
                    f"<port>{port}</port>"
                    f"<log>{log}</log>"
                    f"<transmit>{transmit}</transmit>"
                    f"<send_rate>{send_rate}</send_rate>"
                    f"<verbosity>{verbosity}</verbosity>"
                f"</communication>"
                f"<osc>"
                    f"<base>{osc_base}</base>"
                f"</osc>"
                f"<fusion>"
                    f"<beta_start>1.0</beta_start>"
                    f"<beta_final>0.2</beta_final>"
                    f"<time>5000</time>"
                f"</fusion>"
            f"</configuration>"
            f"<hierarchy>"
                f"<mux Name=\"main\" id=\"0\">{hierarchy[0]}"
                    f"""{str().join([(
                        f'<branch Name="branch" id="1">CH_{i+1}{build_kceptors(branch)}</branch>'
                        if len(branch) > 0 else ""
                    ) for (i, branch) in enumerate(hierarchy[1])])}"""
                f"</mux>"
            f"</hierarchy>"
        f"</chordata>"
    )

from ..utils.NotochordStatus import NotochordStatus

def report_http_response(status, text, method, url):
    report = (
        f"  HTTP error  \n"
        f"==============\n"
        f"{method} {url}\n"
        f"status code: {status}"
    )
    if text:
        report += "\n  " + "\n  ".join(text.split("\n"))
    gui.console_write(report)

def _step_operator(operator, settings, engine):
        status = operator.status
        if status == NotochordStatus.IDLE:
            operator.status = NotochordStatus.CHECKING_CONNECTION
            return (None, None)
        elif status == NotochordStatus.CHECKING_CONNECTION:
            host = None
            port = None
            if settings.use_specified_host:
                host = settings.http_host
                port = settings.http_port
            check = AsyncHTTP.check_connection(host, port)
            if check is None:
                return (None, None)
            if check is False:
                AsyncHTTP.reset_connection_checker()
                operator.status = NotochordStatus.IDLE
                error = "Cannot find Notochord"
                if host is not None:
                    error += "\nUncheck 'use_specified_host' to search Notochord automatically"
                raise Network_error(error)
            if settings.skip_notochord_config:
                operator.status = NotochordStatus.ACTIVATING_CONFIGURATION
                return _step_operator(operator, settings, engine)
            operator.status = NotochordStatus.SENDING_CONFIGURATION
            operator.report({'INFO'}, "Sending configuration")
            gui.console_write("Sending configuration")

            kc_revision = settings.kc_revision
            adapter = settings.adapter
            ip = AsyncHTTP.get_ip()
            port = settings.local_osc_port
            log = settings.log
            transmit = settings.transmit
            send_rate = settings.send_rate
            verbosity = settings.verbosity
            osc_base = settings.osc_base

            data = get_configuration_xml(
                kc_revision, adapter,
                ip, port,
                log, transmit,
                send_rate, verbosity,
                osc_base, engine.engine_root.hierarchy
            )
            return (AsyncHTTP.request(
                "POST", "configuration/blender_config.xml",
                data=data
            ), None)
        elif status == NotochordStatus.SENDING_CONFIGURATION:
            operator.status = NotochordStatus.ACTIVATING_CONFIGURATION
            operator.report({'INFO'}, "Activating configuration")
            gui.console_write("Activating configuration")
            data = """<ControlServerState><NotochordConfigurations>blender_config</NotochordConfigurations></ControlServerState>"""
            return (AsyncHTTP.request(
                "POST", "state",
                data=data
            ), None)
        elif status == NotochordStatus.ACTIVATING_CONFIGURATION:
            operator.status = NotochordStatus.INITIALIZING
            operator.report({'INFO'}, "Initializing")
            gui.console_write("Initializing")
            return (AsyncHTTP.request(
                "GET", f"""notochord/init?addr={
                    AsyncHTTP.get_ip()
                }&port={
                    settings.local_osc_port
                }&scan={
                    settings.scan
                }&verbose={
                    settings.verbosity
                }&raw=True"""
            ), None)
        elif status == NotochordStatus.INITIALIZING:
            operator.status = NotochordStatus.LISTENING
            operator.report({'INFO'}, "Listening OSC")
            gui.console_write("Listening OSC")

            copp_server = COPP_Server(
                port = settings.local_osc_port,
                logger = out.get_logger()
            )
            copp_server.set_base_osc_addr(settings.osc_base)
            copp_server.start()

            return (None, copp_server)

class NotochordSendConfigurationOperator(
    bpy.types.Operator,
    BaseChordataNodeOperator,
    NotochordStatus.Manager
):
    bl_idname = "chordata.notochord_send_config"
    bl_label = "Send Notochord configuration"

    _current_request_id = None

    def _step(self):
        skip_notochord_config = self.id_settings.skip_notochord_config
        self.id_settings.skip_notochord_config = False
        (request_id, _) = _step_operator(self, self.id_settings, self.engine)
        self.id_settings.skip_notochord_config = skip_notochord_config
        self._current_request_id = request_id

    def _execute(self, context):
        self._step()
        return True

    def _modal(self, context, event):
        if self.status != NotochordStatus.ACTIVATING_CONFIGURATION:
            if self._current_request_id is not None:
                request = AsyncHTTP.get_request(self._current_request_id)
                if request is None:
                    return
                status = request[0]
                AsyncHTTP.remove_request(self._current_request_id)
                self._current_request_id = None
                if status < 200 or status >= 400:
                    self.status = NotochordStatus.IDLE
                    self.id_tree.engine_running = False
                    self.id_tree.engine_stop = True
                    report_http_response(*request)
                    return {'STOPPED'}
            self._step()
            return
        else:
            self.status = NotochordStatus.IDLE
            self.id_tree.engine_running = False
            self.id_tree.engine_stop = True
            return {'STOPPED'}
    
    @classmethod
    def poll(cls, context):
        return cls.get_status() == NotochordStatus.IDLE

class ListenOSCOperator(
    bpy.types.Operator,
    BaseChordataNodeOperator,
    NotochordStatus.Manager
):
    bl_idname = "chordata.listen_osc"
    bl_label = "Listen OSC"

    def _execute(self, context):
        self.status = NotochordStatus.INITIALIZING
        (_, copp_server) = _step_operator(self, self.id_settings, self.engine)
        if not copp_server:
            return False
        self.copp_server = copp_server
        return True

    def _modal(self, context, event):
        if self.status == NotochordStatus.STOPPING:
            self.status = NotochordStatus.IDLE
            return {'STOPPED'}
        for packet in self.copp_server.get():
            packet.session_id = self.packet_counter 
            self.engine.engine_root(packet)  
            self.packet_counter +=1

    def _cancel(self, context):
        if hasattr(self, 'copp_server') and self.copp_server is not None:
            self.copp_server.close_and_wait()
        return True

    @classmethod
    def poll(cls, context):
        return cls.get_status() == NotochordStatus.IDLE

class NotochordStartOperator(
    bpy.types.Operator,
    BaseChordataNodeOperator,
    NotochordStatus.Manager
):
    """Parses tree, initializes Notochord and starts communicating with it"""
    bl_idname = "chordata.notochord_start"
    bl_label = "Notochord Start Operator"

    _current_request_id = None
    _stop_request_id = None
    _last_state_request = None

    def _step(self):
        if self.status == NotochordStatus.INITIALIZING:
            self.report({'INFO'}, "Notochord running")
            gui.console_write("Notochord running")
        (request_id, copp_server) = _step_operator(self, self.id_settings, self.engine)
        self._current_request_id = request_id
        self.copp_server = copp_server
        if self.status == NotochordStatus.LISTENING:
            self._last_state_request = time.time_ns() // 1000000

    def _execute(self, context):
        self._step()
        return True

    def _modal(self, context, event):
        if self.status == NotochordStatus.STOPPING:
            if self._current_request_id is not None:
                AsyncHTTP.remove_request(self._current_request_id)
            if self._stop_request_id is not None:
                request = AsyncHTTP.get_request(self._stop_request_id)
                if request is None:
                    return
                status = request[0]
                AsyncHTTP.remove_request(self._stop_request_id)
                if status >= 200 and status < 400:
                    self.status = NotochordStatus.IDLE
                    self.report({'INFO'}, "Notochord stopped")
                    gui.console_write("Notochord stopped")
                    return {'STOPPED'}
                report_http_response(*request)
                self.status = NotochordStatus.LISTENING
                self._stop_request_id = None
            else:
                self._stop_request_id = AsyncHTTP.request("GET", "notochord/end")
                return

        if self.status != NotochordStatus.LISTENING:
            if self._current_request_id is not None:
                request = AsyncHTTP.get_request(self._current_request_id)
                if request is None:
                    return
                status = request[0]
                AsyncHTTP.remove_request(self._current_request_id)
                self._current_request_id = None
                if status < 200 or status >= 400:
                    self.status = NotochordStatus.IDLE
                    self.id_tree.engine_running = False
                    self.id_tree.engine_stop = True
                    report_http_response(*request)
                    return {'STOPPED'}
            self._step()
            return

        if self._current_request_id is not None:
            request = AsyncHTTP.get_request(self._current_request_id)
            if request is not None:
                status = request[0]
                AsyncHTTP.remove_request(self._current_request_id)
                self._current_request_id = None
                if status < 200 or status >= 400:
                    self.status = NotochordStatus.IDLE
                    self.id_tree.engine_running = False
                    self.id_tree.engine_stop = True
                    report_http_response(*request)
                    return {'STOPPED'}
                else:
                    tree = XML.fromstring(request[1])
                    log = tree.find("Log")
                    log_report = ""
                    for entry in log.findall("./*"):
                        log_report += f"\n  {entry.tag}: {entry.text.strip()}"
                    if log_report:
                        gui.console_write(log_report)
                    status = tree.find("NotochordProcess").text
                    if status == "RUNNING":
                        pass
                    elif status == "IDLE" or status == "STOPPED":
                        self.status = NotochordStatus.IDLE
                context.area.tag_redraw()
        else:
            now = time.time_ns() // 1000000
            if now - self._last_state_request >= 2000:
                self._current_request_id = AsyncHTTP.request("GET", "state?clear_registry=true")
                self._last_state_request = now

        for packet in self.copp_server.get():
            packet.session_id = self.packet_counter 
            self.engine.engine_root(packet)  
            self.packet_counter +=1
        
        if self.status == NotochordStatus.IDLE:
            return {'STOPPED'}

    def _cancel(self, context):
        if hasattr(self, 'copp_server') and self.copp_server is not None:
            self.copp_server.close_and_wait()
        return True

    @classmethod
    def poll(cls, context):
        return cls.get_status() == NotochordStatus.IDLE

class NotochordDemoOperator(bpy.types.Operator, BaseChordataNodeOperator):
    """Sends some demo data"""
    bl_idname = "chordata.notochord_demo"
    bl_label = "Notochord Demo Operator"

    data_file = join(dirname(abspath(__file__)), "Chordata_dummy_data.json")
    # base_osc_addr = "/Chordata/q"

    demo_target = DataTarget.Q

    def _execute(self, context):
        self.id_settings.demo_mode = True
        self.count = 0
        self.base_osc_addr = self.id_settings.osc_base
        COPP_Message.update_matcher(self.id_settings.osc_base)
        with open(self.data_file) as d:
            self.tosend = json.load(d)
        return True

    def create_fake_packet(self):
        packet = COPP_Common_packet()
        packet.target = self.demo_target
        for data in self.tosend[self.count % len(self.tosend)]["data"]:
            addr = "{}/{}/{}".format(self.base_osc_addr, self.demo_target.value ,data[0])
            packet._elements.append(COPP_Message(addr, ",ffff", data[1:]))
            
        return packet

    @staticmethod
    def get_quaternion_at_frame(frame, group):
        q = [0] * 4
        for fc in group.channels:
            q[fc.array_index] = fc.evaluate(frame)

        return q

    def create_action_packet(self):

        if self.engine.engine_root.id_settings.demo_anim != 'CUSTOM':
            action = bpy.data.actions[self.engine.engine_root.id_settings.demo_anim]
        else:
            action = self.engine.engine_root.id_settings.custom_action

        duration = action.frame_range[1] - action.frame_range[0]
        frame =  action.frame_range[0] + (self.count % duration)

        packet = COPP_Common_packet()
        packet.target = self.demo_target
        for target_group in action.groups:
            if not target_group.name.startswith(POSE_SPACE_G_SUFX ):
                continue
            q = self.get_quaternion_at_frame(frame, target_group)
            addr = "{}/{}/{}".format(self.base_osc_addr, self.demo_target.value, target_group.name[len(POSE_SPACE_G_SUFX ):])
            packet._elements.append(COPP_Message(addr, ",ffff", q))

        return packet

    def _modal(self, context, event):
        if self.count == 0:
            self.id_tree.engine_running = True
            self.id_tree.engine_stop = False
        elif not self.id_tree.engine_running:
            return {'STOPPED'}

        self.count += 1
        if self.engine.engine_root.id_settings.demo_fake:
            packet = self.create_fake_packet()
        else:
            packet = self.create_action_packet()

        packet.session_id = self.packet_counter 
        self.packet_counter += 1

        self.engine.engine_root( packet )

def notochord_stop_demo(tree):
    tree.engine_running = False
    tree.engine_stop = True

class NotochordStopDemoOperator(
    bpy.types.Operator,
    ID_data_settings_getter
):
    """Stop engine"""
    bl_idname = "chordata.notochord_stop_demo"
    bl_label = "Notochord Stop Demo Operator"

    from_tree: bpy.props.StringProperty()
    from_node: bpy.props.StringProperty()

    def execute(self, context):
        notochord_stop_demo(self.id_tree)
        return {'FINISHED'}

def notochord_stop():
    NotochordStatus.Manager.set_status(NotochordStatus.STOPPING)

class NotochordStopOperator(
    bpy.types.Operator,
    NotochordStatus.Manager
):
    """Stop engine"""
    bl_idname = "chordata.notochord_stop"
    bl_label = "Notochord Stop Operator"

    def execute(self, context):
        self.report({'INFO'}, "Requesting Notochord stop")
        gui.console_write("Requesting Notochord stop")
        notochord_stop()
        return {'FINISHED'}

    @classmethod
    def poll(cls, context):
        return cls.get_status() == NotochordStatus.LISTENING

class ActionPoseRotationItem(bpy.types.PropertyGroup):
    name: bpy.props.StringProperty(name="Bone name", default="unset")
    value: bpy.props.FloatVectorProperty(name="Pose space rotation", 
        description="Rotation in pose space of the associated bone", default=(1, 0, 0, 0),
        size=4, subtype='QUATERNION')

def add_props_to_Action():
    bpy.types.Action.chordata_world_rotations = bpy.props.CollectionProperty(
        type=ActionPoseRotationItem, options={'ANIMATABLE'})

to_register = (
    NotochordReceiveOperator,
    NotochordStartOperator,
    NotochordStopOperator,
    NotochordDemoOperator,
    NotochordStopDemoOperator,
    NotochordSendConfigurationOperator,
    ListenOSCOperator,
    ActionPoseRotationItem
)
