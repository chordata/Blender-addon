import bpy
from .. import defaults

class BakePoseSpaceRotationInAction(bpy.types.Operator):
    """Bakes the rotation_quaternion data path of each bone in the corresponding
    Pose space rotation, using different channels in the same action"""

    bl_idname = "chordata.bake_pose_space_rotation"
    bl_label = "Bake pose space bone rotations"

    _timer = None

    def modal(self, context, event):
        armature_ob = self.armature_ob
        action = self.action

        if event.type in {'RIGHTMOUSE', 'ESC'}:
            self.cancel(context)
            return {'CANCELLED'}

        if event.type == 'TIMER':
            frame =  action.frame_range[0] + (self.count)

            self.count += 1
            bpy.context.scene.frame_current = frame
            for bone in armature_ob.pose.bones:
                bone.rotation_q_pose_space = bone.matrix.to_quaternion().copy()

                key_insert = armature_ob.keyframe_insert(\
                        'pose.bones["{}"].rotation_q_pose_space'.format(bone.name),
                            index=-1, 
                            frame=frame, 
                            # group= defaults.POSE_SPACE_G_SUFX + bone.name)
                            group= "SOME GROUP")

                
                print(frame, key_insert, defaults.POSE_SPACE_G_SUFX + bone.name, bone.rotation_q_pose_space)
            
            if frame >= action.frame_range[1]:
                self.cancel(context)
                return {'FINISHED'}

        return {'PASS_THROUGH'}


    def execute(self, context):
        if not context.object:
            self.report({'ERROR'}, "No selected object")
            return {'CANCELLED'}

        if type(context.object.data) != bpy.types.Armature:
            self.report({'ERROR'}, "The selected object is not of Armature type")
            return {'CANCELLED'}
        
        self.armature_ob = context.object
        armature_ob = self.armature_ob
        
        if not armature_ob.animation_data or not armature_ob.animation_data.action:
            self.report({'ERROR'}, "The selected object doesn't have an action assigned")
            return {'CANCELLED'}

        self.action = armature_ob.animation_data.action
        action = self.action
        print("Baking pose space rotation of action:", action)

        self.count = 0
        self.duration = int(action.frame_range[1] - action.frame_range[0])
    
        for bone in armature_ob.data.bones:
            bone.use_inherit_rotation = True

        self.starting_frame = bpy.context.scene.frame_current

        
        wm = context.window_manager
        self._timer = wm.event_timer_add(0.05, window=context.window)
        wm.modal_handler_add(self)
        return {'RUNNING_MODAL'}

    def cancel(self, context):
        wm = context.window_manager
        wm.event_timer_remove(self._timer)
        bpy.context.scene.frame_current = self.starting_frame    


to_register = (BakePoseSpaceRotationInAction,)