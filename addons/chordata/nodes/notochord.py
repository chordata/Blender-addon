# Chordata client (Blender addon)  
# -- Motion capture manager for the Chordata Open Source motion capture system
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2019 Bruno Laurencich
# Copyright 2020-2022 Bruno Laurencich, Lorenzo Micozzi Ferri
#
# This file is part of Chordata client (Blender addon).
#
# Chordata client (Blender addon) is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata client (Blender addon).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

import bpy
import bpy.props
from bpy.types import NodeSocket
from .basenode import ChordataBaseNode, set_dirty_flag
from ..ops import engine, notochord_ops
from .. import defaults

from ..utils import gui
from ..utils.NotochordStatus import NotochordStatus

import time

def create_anims_enum(self, context):
    items = []

    sufx_len = len(defaults.DEMO_ANIM_SUFX)
    for anim in bpy.data.actions:
        if anim.is_chordata_item:
            items.append((anim.name, anim.name[sufx_len:], "Full body mocap animation"))
    
    items.append(("FAKE-ARM", "Fake arm", "A perfecly looping, synthetic arm animation"))
    items.append(("CUSTOM", "Custom Action", "Choose an action within the ones in this .blend file"))
    
    return items

def check_fake_action(self, context):
    if self.demo_anim == "FAKE-ARM":
        self.demo_fake = True
    else:
        self.demo_fake = False

    set_dirty_flag(self, context)


class NotochordSettings(bpy.types.PropertyGroup):
    # === Custom Properties ========================================================
    local_address: bpy.props.StringProperty(
        name="Local IP address", default="127.0.0.1"
    )
    scan: bpy.props.BoolProperty(
        name="Scan KCeptors", default=False)
    local_osc_port: bpy.props.IntProperty(
        name="Local OSC receiving Port", default=6565)
    config_file: bpy.props.PointerProperty(
        name="Armature Config", type=bpy.types.Text)
    notochord_address: bpy.props.StringProperty(
        name="Notochord Address", default="127.0.0.1")
    show_advanced_network: bpy.props.BoolProperty(
        name="Show Advanced Network", default=False)
    show_advanced: bpy.props.BoolProperty(
        name="Show Advanced", default=False)

    # advanced network
    transmission_mode: bpy.props.EnumProperty(
        name="Transmission mode",
        description="Network transmission mode used",
        items=[
            ("UNICAST", "Unicast", "For notochord Unicast transmission"),
            ("BROADCAST", "Broadcast", "For notochord Broadcast transmission"),
            ("MULTICAST", "Multicast", "For notochord Multicast transmission")
        ],
    )
    notochord_port: bpy.props.IntProperty(name="Notochord Port", default=80)
    net_submask: bpy.props.StringProperty(
        name="Net Submask", default="255.255.255.0")
    multi_id_group: bpy.props.StringProperty(
        name="Multi_id Group", default="239.0.0.1")

    # advanced
    kc_revision: bpy.props.EnumProperty(
        name="KCeptor revision", default="++",
        items=[("2", "2", "2"), ("++", "++", "++")]
    )
    log: bpy.props.StringProperty(name="Log", default="stdout,file")
    transmit: bpy.props.StringProperty(name="Transmit", default="osc")
    send_rate: bpy.props.EnumProperty(
        name="Send rate", default="50",
        items=[("50", "50", "50"), ("90", "90", "90")]
    )
    verbosity: bpy.props.IntProperty(name="Verbosity", default=0, min=0, max=2)
    adapter: bpy.props.StringProperty(name="Adapter", default="/dev/i2c-1")
    osc_base: bpy.props.StringProperty(name="OSC base", default="/Chordata")

    use_specified_host: bpy.props.BoolProperty(
        name="Use specified HTTP host:port", default=False)
    http_host: bpy.props.StringProperty(name="HTTP host", default="notochord")
    http_port: bpy.props.IntProperty(name="HTTP port", default=80, step=1, min=1, max=65535)

    skip_notochord_config: bpy.props.BoolProperty(name="Skip Notochord configuration", default=False)

    manual_override: bpy.props.BoolProperty(
        name="Manual Notochord Override", default=False)

    # Demo mode
    demo_mode: bpy.props.BoolProperty(
        name="Demo Mode", default=False, update=set_dirty_flag)
    
    demo_anim: bpy.props.EnumProperty(
            name="Demo action",
            description="Action to output in Demo mode",
            # update=set_dirty_flag,
            items=create_anims_enum,
            update=check_fake_action
    )

    demo_fake: bpy.props.BoolProperty(default=True)

    custom_action: bpy.props.PointerProperty(
        name="Custom Action", type=bpy.types.Action)
     

    # ==============================================================================


class NotochordNode(ChordataBaseNode):

    '''Chordata Notochord Node'''
    bl_idname = 'NotochordNodeType'

    bl_label = "Notochord Node"

    # === Property Group Pointer ===================================================
    settings: bpy.props.PointerProperty(
        type=NotochordSettings)
    # ==============================================================================

    def init(self, context):
        self.width = 400.0

        self.inputs.new('HierarchySocketType', "hierarchy")

        self.outputs.new('DataStreamSocketType', "capture_out")
        self.init_stats()

        chordata_demo_action = False
        for anim in bpy.data.actions:
            if anim.is_chordata_item:
                chordata_demo_action = anim.name
                break

        if not chordata_demo_action:
            bpy.ops.chordata.actions_add('EXEC_DEFAULT')
            for anim in bpy.data.actions:
                if anim.is_chordata_item:
                    chordata_demo_action = anim.name
                    break

        self.settings.demo_fake = not bool(chordata_demo_action)
                
        if not chordata_demo_action:
            chordata_demo_action = "FAKE-ARM"
        self.settings.demo_anim = chordata_demo_action

    # Free function to clean up on removal.
    def free(self):
        node_tree = self.id_data
        if node_tree.engine_running:
            if self.settings.demo_mode:
                notochord_ops.notochord_stop_demo(node_tree)
            else:
                notochord_ops.notochord_stop()

    def draw_buttons(self, context, layout):
        if self.settings.demo_mode:
            row = layout.row()
            icon = False
            if self.id_data.engine_running:
                now = time.time_ns() // 1000000
                icon = ((now % 1000) / 500) < 1
            row.column().label(
                icon="DECORATE_KEYFRAME" if icon else "DECORATE_ANIMATE"
            )
            row.column().label(text="DEMO")
            row.column().separator()
            row.column().prop(
                self.settings, "demo_mode",
                icon='CON_FOLLOWPATH'
            )

            gui.label_multiline(
                width=self.width - (2 * gui.char_width),
                text="In this mode the notochord node will output a pre-recorded sample capture",
                parent=layout.column()
            )

            row = layout.row()
            row.scale_y = 1.5
            if self.id_data.engine_running:
                stop_op = row.operator(
                    "chordata.notochord_stop_demo", text="Stop Demo")
                stop_op.from_tree = self.id_data.name
                stop_op.from_node = self.name
            else:
                receive_op = row.operator("chordata.notochord_demo",
                                        text="Run Demo")
                receive_op.from_tree = self.id_data.name
                receive_op.from_node = self.name

            layout.prop(self.settings, "demo_anim")
            
            if self.settings.demo_anim == 'CUSTOM':
                layout.prop(self.settings, "custom_action")
        else:
            status = NotochordStatus.Manager.get_status()

            row = layout.row()
            label = "IDLE"
            icon = False
            if status == NotochordStatus.STOPPING:
                label = "STOPPING"
                icon = True
            elif status != NotochordStatus.IDLE:
                if status == NotochordStatus.CHECKING_CONNECTION:
                    label = "CHECKING CONNECTION"
                elif status == NotochordStatus.SENDING_CONFIGURATION:
                    label = "SENDING CONFIGURATION"
                elif status == NotochordStatus.ACTIVATING_CONFIGURATION:
                    label = "ACTIVATING CONFIGURATION"
                elif status == NotochordStatus.INITIALIZING:
                    label = "INITIALIZING"
                elif status == NotochordStatus.LISTENING:
                    label = "LISTENING"
                now = time.time_ns() // 1000000
                icon = ((now % 1000) / 500) < 1
            row.column().label(
                icon=("RADIOBUT_ON" if icon else "RADIOBUT_OFF")
            )
            row.column().label(text=label)
            row.column().separator()
            row.column().prop(
                self.settings, "demo_mode",
                icon='CON_FOLLOWPATH'
            )

            layout.prop(self.settings, "kc_revision")
            layout.prop(self.settings, "send_rate")
            layout.prop(self.settings, "local_osc_port")

            layout.separator()

            layout.prop(self.settings, "scan")

            layout.separator()

            row = layout.row(align=True)
            row.scale_y = 1.5
            if status < NotochordStatus.LISTENING:
                receive_op = row.operator("chordata.notochord_start",
                                        text="Connect")
                receive_op.from_tree = self.id_data.name
                receive_op.from_node = self.name
            else:
                stop_op = row.operator(
                    "chordata.notochord_stop", text="Disconnect")

            layout.separator()

            layout.prop(self.settings, "show_advanced")
            if self.settings.show_advanced:
                box = layout.box()

                box.label(
                    text="CHANGE THESE ONLY IF YOU KNOW WHAT YOU'RE DOING!")

                box.separator()

                box.prop(self.settings, "use_specified_host")
                row = box.row(align=True)
                col = row.column()
                col.alignment = "RIGHT"
                col.label(text="http://")
                col = row.column()
                col.alignment = "EXPAND"
                col.prop(self.settings, "http_host", text="", icon_only=True, expand=True)
                col = row.column()
                col.alignment = "CENTER"
                col.label(text=":")
                col = row.column()
                col.alignment = "RIGHT"
                col.prop(self.settings, "http_port", text="", expand=False)
                row.enabled = self.settings.use_specified_host

                box.separator()

                box.prop(self.settings, "adapter")
                box.prop(self.settings, "osc_base")
                box.prop(self.settings, "log")
                box.prop(self.settings, "transmit")
                box.prop(self.settings, "verbosity")

                box.separator()

                box.prop(self.settings, "skip_notochord_config")

                box.separator()

                row = box.row()
                row.scale_y = 1.5
                op = row.operator("chordata.notochord_send_config")
                op.from_tree = self.id_data.name
                op.from_node = self.name

                box.separator()

                row = box.row()
                row.scale_y = 1.5
                op = row.operator("chordata.listen_osc")
                op.from_tree = self.id_data.name
                op.from_node = self.name

    def draw_label(self):
        if self.settings.manual_override:
            return "Notochord Node (Manual Override ON!)"
        elif self.settings.show_advanced:
            return "Notochord Node (Advanced)"
        else:
            return "Notochord Node"

    @engine.datatarget_handler(engine.DataTarget.Q)
    def q_handler(self, packet):
        return packet

    
to_register = (NotochordSettings, NotochordNode)
