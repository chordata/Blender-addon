# Chordata client (Blender addon)  
# -- Motion capture manager for the Chordata Open Source motion capture system
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2019 Bruno Laurencich
# Copyright 2020-2022 Bruno Laurencich, Lorenzo Micozzi Ferri
#
# This file is part of Chordata client (Blender addon).
#
# Chordata client (Blender addon) is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata client (Blender addon).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

import bpy
from bpy.types import NodeSocket
from .basenode import ChordataBaseNode
from ..ops import engine
from ..utils import gui




# def set_action(self, context):
#     return
#     # if not self.action:
    #     self.action = bpy.data.actions.new("Chordata_capture")
    #     self.action.use_fake_user = True

    # try:
    #     if self.target_object:
    #         if not self.target_object.animation_data:
    #             self.target_object.animation_data_create()

    #         if self.target_object.animation_data.action:
    #             self.target_object.animation_data.action.use_fake_user = True

    #         self.target_object.animation_data.action = self.action
        
    # except Exception as e:
    #     print("Exception in REC node target_ob update handler:\n", e)


class RecordSettings(bpy.types.PropertyGroup):
    # === Custom Properties ========================================================
    # action: bpy.props.PointerProperty(type=bpy.types.Action, name="Action")
    do_record: bpy.props.BoolProperty(name="Record Capture")
    target_object: bpy.props.PointerProperty(
        type=bpy.types.Object, name="Target object")
    create_new_action: bpy.props.BoolProperty(name="Create new action", default=True)
    armature_datasocket_alert: bpy.props.BoolProperty(default=False)


    # ==============================================================================


class RecordNode(ChordataBaseNode):

    '''Records Chordata capture to action data block'''
    bl_idname = 'RecordNodeType'

    bl_label = "Record Node"

    # === Property Group Pointer ===================================================
    settings: bpy.props.PointerProperty(
        type=RecordSettings)
    # ==============================================================================

    def update(self):
        super().update()
        self.check_armature_socket()
    

    def init(self, context):
        self.width = 350.0
        self.inputs.new('DataStreamSocketType', "data_in")
        self.inputs.new('ArmatureObjectSocketType', "armature_in")
        self.outputs.new('DataStreamSocketType', "data_out")
        self.init_stats()

        # if not self.settings.action:
        #     self.settings.action = bpy.data.actions.new("Chordata_capture")
        #     self.settings.action.use_fake_user = True

    def draw_buttons(self, context, layout):
        layout.prop(self.settings, "target_object", text="Target object")

        if self.settings.target_object:
            if self.settings.target_object.animation_data and self.settings.target_object.animation_data.action: 
                layout.prop(self.settings.target_object.animation_data, "action", text="Active action")
                layout.prop(self.settings, "create_new_action", text="Create new action for each record")
        else:
            layout.label(text = "Select a target object to place the recorded action")

        # layout.separator()
        row = layout.row(align=True)
        row.prop(self.settings, "do_record", text="Record", icon="REC", expand=True)
        row.scale_y = 1.5
        if self.id_data.engine_running:
            row.active = True  
        elif self.settings.target_object:
            row.active = False  
            if self.settings.target_object.animation_data and self.settings.target_object.animation_data.action: 
                if context.screen.is_animation_playing:
                    layout.operator("screen.animation_play", text="Pause playing", icon="PAUSE")
                else:
                    layout.operator("screen.animation_play", text="Play", icon="PLAY")

        if self.settings.armature_datasocket_alert:
            box = layout.box()
            box.alert = True
            box.label(text="Use the `Armature in` socket to receive data from an Armature node")


    def draw_label(self):
        return "Record Node"

    def on_engine_node_init(self):
        self.check_armature_socket()
        self._recording = False
        self.fps = bpy.context.scene.render.fps
        self.frame_offset = 0


    @engine.helper_method
    def check_armature_socket(self):
        #this method is also executed from the real node,
        #so patch the `self` and `id_settings` prop in that case
        if not hasattr(self, "inputs"):
            self = self.id_node

        if not hasattr(self, "id_settings"):
            self.id_settings = self.settings

        self.id_settings.armature_datasocket_alert = False
        for inp in self.inputs:
            for link in inp.links:
                if link.from_node.bl_idname == 'ArmatureNodeType':
                    if link.from_socket.bl_idname == 'DataStreamSocketType':
                        self.id_settings.armature_datasocket_alert = True
                    else:
                        if link.from_node.settings.valid_arm_ob:
                            self.id_settings.target_object = link.from_node.settings.armature_ob


    @engine.helper_method
    def check_recording_state(self):
        if not self._recording:
            self._recording = True
            self.stats.restart()
            anim_data = self.id_settings.target_object.animation_data 
            if anim_data and anim_data.action:
                if self.id_settings.create_new_action:
                    anim_data.action.use_fake_user = True
                    anim_data.action = None
                    self.frame_offset = 0
                else:
                    self.frame_offset = anim_data.action.frame_range[1]
            else:
                self.frame_offset = 0

    @engine.helper_method
    def get_frame(self):
        frame= int(self.stats.ellapsed_time() * self.fps) + int(self.frame_offset)
        bpy.context.scene.frame_current = frame
        return frame

    @engine.datatarget_handler(engine.DataTarget.Q)
    def process_q(self, packet):
        if self.id_settings.do_record and self.id_settings.target_object:
            delta = self.stats.get_checkpoint_delta()
            if delta < (1/self.fps):
                return packet

            self.check_recording_state()
            frame = self.get_frame()
            ob = self.id_settings.target_object

            if ob.type == 'MESH':
                gui.console_write("In TestCube" + packet._test_cube_target_)

                key_insert = ob.keyframe_insert(\
                    'rotation_quaternion',
                    index=-1,
                    frame=frame,
                    group='rotation')

            elif ob.type == 'ARMATURE':
                
                for bone in ob.pose.bones:
                    if bone.chordata.output_dirty:
                        key_insert = ob.keyframe_insert(\
                            'pose.bones["{}"].rotation_quaternion'.format(bone.name),
                                index=-1, 
                                frame=frame, 
                                group=bone.name)

                        bone.chordata.output_dirty = False

                key_insert = ob.keyframe_insert(\
                            'location',
                            index=-1, 
                            frame=frame, 
                            group='location')

    @engine.datatarget_handler(engine.DataTarget.ROT)
    def rec_rot(self, packet):
        if self.id_settings.do_record and self.id_settings.target_object:
            self.check_recording_state()
            frame = self.get_frame()
            ob = self.id_settings.target_object

            for msg in packet.get():
                # gui.console_write( "REC: (@{:6.2f} | F:{:4d}) [{:>15}] > {}".format(self.stats.ellapsed_time(), 
                #     frame, msg.subtarget, msg.payload) )
                key_insert = ob.keyframe_insert(\
                    'pose.bones["{}"].rotation_quaternion'.format(msg.subtarget),
                        index=-1, 
                        frame=frame, 
                        group=msg.subtarget)

            key_insert = ob.keyframe_insert(\
                        'location',
                        index=-1, 
                        frame=frame, 
                        group='location')


        else:
            self._recording = False


to_register = (RecordSettings, RecordNode,)
