from bpy.types import Node
from bpy.props import StringProperty, IntProperty
from ..chordatatree import ChordataTreeNode

class MuxNode(Node, ChordataTreeNode):
    bl_idname = 'MuxNodeType'
    bl_label = "Mux Node"

    address: StringProperty(name="Address", default="0x70")

    def init(self, context):
        self.outputs.new('HierarchySocketType', "hierarchy")

        self.inputs.new("KCeptorSocketType", 'CH 1')
        self.inputs.new("KCeptorSocketType", 'CH 2')
        self.inputs.new("KCeptorSocketType", 'CH 3')
        self.inputs.new("KCeptorSocketType", 'CH 4')
        self.inputs.new("KCeptorSocketType", 'CH 5')
        self.inputs.new("KCeptorSocketType", 'CH 6')
    
    def draw_buttons(self, context, layout):
        layout.prop(self, "address", expand=False)

class KCeptorNode(Node, ChordataTreeNode):
    bl_idname = 'KCeptorNodeType'
    bl_label = "KCeptor Node"

    address: IntProperty(
        name="Address", default=64,
        min=64, max=95, step=1
    )
    name: StringProperty(name="Name", default="k_ceptor")

    def init(self, context):
        self.outputs.new("KCeptorSocketType", "downstream")
        self.inputs.new("KCeptorSocketType", "upstream")
    
    def draw_buttons(self, context, layout):
        layout.prop(self, "address", expand=True)
        row = layout.row()
        col = row.column()
        col.label(text="  Address hex:")
        col = row.column()
        col.alignment = "RIGHT"
        col.label(text=f"{hex(self.address)}  ")

        layout.separator()

        layout.prop(self, "name")

to_register = (MuxNode, KCeptorNode)